#include <iostream>
#include <math.h>

//Crear un programa usando POO para calcular el Area y Perimetro de un triangulo

class Triangulo{
private:
  /* atributos privados de la clase */
  float lado;

public:
  Triangulo(float);
  void MostrarPerimetro_Area();
};

Triangulo::Triangulo(float _lado){
  lado=_lado;
}
void Triangulo::MostrarPerimetro_Area(){
    float resultado = lado*3;
    float area = (sqrt(3)/4)*lado;
    std::cout << "Perimetro : "<< resultado <<" unidades" << '\n';
    std::cout << "Area      : "<< area << " unidades cuadradas"<<'\n';
    return;
}

int main() {
  float LadoTriangulo=0;
  std::cout << "Ingrese el lado del triangulo equilatero " << '\n';
  std::cin >> LadoTriangulo;
  Triangulo objeto = Triangulo(LadoTriangulo);
  objeto.MostrarPerimetro_Area();
  return 0;
}
