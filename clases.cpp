#include <iostream>
#include <string>
using namespace std;


class Personas {
private:
  /* datos privados de la clase */
  string nombre;
  string apellido;
  int edad;
public:
  Personas (string,string,int); //constructor de la clase Personas
  void mostrarInformacion();
};
//constructor de la clase personas //
Personas::Personas(string _nombre , string _apellido, int _edad ){
  nombre= _nombre;
  apellido= _apellido;
  edad= _edad;
}
void Personas::mostrarInformacion(){
  /* metodo mostrar informacion*/
  std::cout << "Nombre   : "<< nombre << '\n';
  std::cout << "Apellido : "<< apellido << '\n';
  std::cout << "Edad     : "<< edad << '\n';
  return;
  
}

int main() {
  /* Primer ejemplo de una clase en c++ */
  Personas julian = Personas("julian","rugeles",25);
  julian.mostrarInformacion();
  return 0;
}
