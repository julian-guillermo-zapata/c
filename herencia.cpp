#include <iostream>
// ejemplo herencia POO
// julian Guillermo Zapata Rugeles


// --------------------- Inicio clases super -----------------------------//


class Personas {
private:
  /* atributos */
  std::string nombre;
  int edad;

public:
  Personas(std::string , int );
  void mostrarInformacion();
};
// constructor de la super clase ###### CONSTRUCTO ######
Personas::Personas(std::string _nombre , int _edad ){
  nombre = _nombre;
  edad = _edad;
}
void Personas::mostrarInformacion(){
  /* mostrar informacion super clase */
  std::cout << "Nombre :"<< nombre << '\n';
  std::cout << "Edad   :"<< edad << '\n';
  return;
}
 // -------------------- fin super clase ----------------------------------//
// clase hija -------------------------------------------------------------//

class Empleado : public Personas{
private:
  /* atributos de la subclase Empleado */
  bool status_Empleado;
public:
  Empleado (std::string,int,bool);
  void mostrarEmpleado();
};

Empleado::Empleado(std::string _nombre ,int _edad ,bool _status_Empleado = false ): Personas(_nombre,_edad){
  status_Empleado=_status_Empleado;
}

void Empleado::mostrarEmpleado(){
  mostrarInformacion();
  std::cout << "Empleado ? : "<< status_Empleado << '\n';
  return;
}
// ------------------------- fin sub clase Empleado ------------------------------


int main() {
  std::cout << "---------Ejemplo de herencia---------" << '\n';
  Empleado julian = Empleado("julian",22,true);
  std::cout << "Informacion basica " << '\n';
  julian.mostrarInformacion();
  std::cout << "\n \nInformacion completa " << '\n';
  julian.mostrarEmpleado();
  return 0;
}
